var express = require('express');
var createError = require('http-errors');
var router = express.Router()
const Mongo = require('../bin/mongo') ;
const ObjectId = require('mongodb').ObjectId;

/* lister les différents flipBook */
router.get('/', function(req, res, next) {
  Mongo.getInstance().collection('flipbook').find({status:"published"}).toArray((err, books)=> {
    res.render('index', {title:"Bienvenue dans notre  bibliotheque collaborative ", books:books});
  })
});

/* retourner en JSON les données liée à un flipBook dont l'id est dans l'url */
router.get('/:id', function(req, res, next) {
  Mongo.getInstance().collection('flipbook').findOne({_id:new ObjectId(req.params.id), status:"published"}, (err, book)=> {
    if(err)
      throw err ;
    if(!book._id)
      return next(createError(404));
    res.render('flipbook', {title:"Bienvenue dans notre  bibliotheque collaborative ", book:book});
  })
});

module.exports = router;
